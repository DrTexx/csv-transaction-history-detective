import os
import pytest
from csvthd.cli import cli


def test_exit_if_transactions_empty(capsys):
    os.environ[
        "CSVTHD_CONFIG_FILEPATH"
    ] = "./tests/sample_data/empty_transactions/config.json"

    with pytest.raises(SystemExit) as e:
        cli()

    assert e.value.code > 0
