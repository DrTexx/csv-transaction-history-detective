from functools import reduce
from csvthd.config import load_config
from csvthd.transactions import get_transactions
from csvthd.reducers import sum_transaction_amount


def test_sum_transaction_amount():
    config = load_config("./tests/sample_data/realistic/config.json")
    transactions = get_transactions(config["files"])
    transactions, sum_amount = reduce(
        sum_transaction_amount, transactions, [[], 0]
    )
