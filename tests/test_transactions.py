# site
import pytest
from csvthd.config import load_config
from csvthd.transactions import get_transactions
from csvthd.exceptions import EmptyTransactionFile


def test_get_transactions():
    config = load_config("./tests/sample_data/realistic/config.json")

    get_transactions(config["files"])


def test_get_transactions_handle_empty():
    config = load_config("./tests/sample_data/empty_transactions/config.json")

    with pytest.raises(EmptyTransactionFile):
        get_transactions(config["files"])


def test_get_transactions_complex():
    config = load_config("./tests/sample_data/realistic_complex/config.json")

    get_transactions(config["files"])
