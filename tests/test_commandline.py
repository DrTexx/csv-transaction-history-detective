from csvthd.commandline import style_amount, print_transaction

RED_BACK = "\033[41m"
GREEN_BACK = "\033[42m"
ANSI_RESET = "\033[0m"


def test_style_amount():
    above_zero_styled = style_amount(10)
    zero_styled = style_amount(0)
    below_zero_styled = style_amount(-10)

    assert above_zero_styled == GREEN_BACK + "   10.00" + ANSI_RESET
    assert zero_styled == GREEN_BACK + "    0.00" + ANSI_RESET
    assert below_zero_styled == RED_BACK + "  -10.00" + ANSI_RESET


def test_print_transaction(capsys):
    date_str = "01/01/2022"
    amount = 10
    details = "A really cool transaction"
    account_name = "MyBank Everyday Account"

    print_transaction(
        {
            "date": date_str,
            "amount": amount,
            "details": details,
            "account_name": account_name,
        }
    )
    assert (
        capsys.readouterr().out
        == f"[ MyBank Everyday Account ] 01/01/2022 | AMT: {GREEN_BACK}   10.00{ANSI_RESET} | A really cool transaction\n"
    )
