[![pipeline status](https://gitlab.com/DrTexx/csv-transaction-history-detective/badges/main/pipeline.svg)](https://gitlab.com/DrTexx/csv-transaction-history-detective/-/commits/main)
[![coverage report](https://gitlab.com/DrTexx/csv-transaction-history-detective/badges/main/coverage.svg)](https://gitlab.com/DrTexx/csv-transaction-history-detective/-/commits/main)
[![GitLab issues](https://img.shields.io/gitlab/issues/open/drtexx/csv-transaction-history-detective?logo=gitlab)](https://gitlab.com/DrTexx/csv-transaction-history-detective/-/issues)
[![GitLab merge requests](https://img.shields.io/gitlab/merge-requests/open/drtexx/csv-transaction-history-detective?logo=gitlab)](https://gitlab.com/DrTexx/csv-transaction-history-detective/-/merge_requests)

# CSV Transaction History Detective

[![PyPI Release](https://img.shields.io/pypi/v/csvthd?logo=python)](https://pypi.org/project/csvthd)
[![PyPI - Downloads](https://img.shields.io/pypi/dm/csvthd?logo=Python)](https://pypi.org/project/csvthd)
[![PyPI - License](https://img.shields.io/pypi/l/csvthd?color=orange&logo=Python)](https://pypi.org/project/csvthd)

## Getting started

<!-- TODO: add an example `config.json` file called `config.json.example.json` -->

<!-- TODO: add an example csv file to give more context to why `config.json.example.md` is laid out how it is -->

<!-- TODO: mention in `config.json.template.md` to look at `config.json.example.md` for an example config. -->

1. [Install](https://gitlab.com/DrTexx/csv-transaction-history-detective/#installation) `csvthd`
2. Create your `config.json` file (see [`config.json.template.md`](https://gitlab.com/DrTexx/csv-transaction-history-detective/-/blob/main/config.json.template.md) for a template)
3. See [usage](https://gitlab.com/DrTexx/csv-transaction-history-detective/#usage)

## Installation

```bash
pip install csvthd
```

<!-- TODO: add a "features" section that highlights the capabilities of csvthd in a non-technical way. i.e. filtering/sorting/reporting/etc. options -->

## Usage

### Show help

```bash
csvthd --help
```

### Filter

#### Transaction details

`-i/--include`

Only show transactions that include **all** of the specified strings in their details

- Case insensitive
- Multiple strings supported (each transaction's details must include **all** of the strings specified)

```bash
# only show transactions with details including the word "paypal"
csvthd -i paypal

# only show transactions with details including "paypal" and "steam"
csvthd -i paypal -i steam
```

`-E/--exclude`

Only show transactions that **don't** include **any** of the specified strings in their details

- Case insensitive
- Multiple strings supported (each transaction's details mustn't include **any** of the strings specified)

```bash
# only show transactions without details containing the word "paypal"
csvthd -E paypal

# only show transactions without details containing "paypal" or "chemist warehouse"
csvthd -E paypal -E "chemist warehouse"

# only show transactions with details containing "paypal", but not "steam"
csvthd -i paypal -E steam
```

#### Amount

`-a/--amount`

Only show transactions with amounts under/over/equal to a given value

- Multiple numbers supported (each transaction amount must satisfy **all** conditions specified)

```bash
# only show transactions over $20.00
csvthd -a over 20

# only show transactions under $10.00
csvthd -a under 10

# only show transactions between $20.00 to $30.00
csvthd -a over 20 -a under 30

# only show transactions of exactly $25.00
csvthd -a equal 25
```

#### Date

`-d/--date`

Only show transactions before/after/on a given date.

```bash
# only show transactions after 1 Jan 2022
csvthd -d after 01/01/2022

# only show transactions before 21 Feb 2022
csvthd -d before 21/02/2022

# only show transactions on 10 Apr 2022
csvthd -d on 10/04/2022

# only show transactions from 1 Jan 2022 - 1 Feb 2022
csvthd -d after 01/01/2022 -d before 01/02/2022
```

#### Type

`-t/--transaction-type`

Only show transactions where money is sent/received

```bash
# only show transactions where money is sent
csvthd -t out

# only show transactions where money is received
csvthd -t in

# only show transactions with details containing "paypal" where money received
csvthd -i paypal -t in

# get the sum of all money received with "paypal" in details but not "return"
csvthd -i paypal -t in -E return -S
```

#### Account Name

`-A/--account-name`

Only show transactions from account names that include the provided text.

```bash
# only show transactions where the account name includes "commbank"
csvthd -A commbank

# get the sum of all transactions where the account name includes "paypal"
csvthd -A paypal -S
```

### Sorting

#### Sort by

`-s/--sort-by`

```bash
# list transactions from latest to oldest (default)
csvthd -s date

# list transactions from lowest to highest
csvthd -s amount
```

#### Reverse sorting order

`-r/--reverse-sort`

```bash
# list latest transactions first
csvthd

# list oldest transactions first
csvthd -r

# list smallest transactions first
csvthd -s amount

# list largest transactions first
csvthd -s amount -r
```

### Reports

#### Sum amount

`-S/--sum`

Show the sum of the transaction amounts (after filtering)

```bash
# print the sum of all transactions
csvthd -S

# get the sum of transactions with "paypal" in their details
csvthd -S -i paypal
```

## Development

### Build

```bash
./development-scripts/build.sh
```

## Links

<!-- TODO: add website link -->
- 📖 &nbsp;[Documentation](https://gitlab.com/DrTexx/csv-transaction-history-detective)
- 🐍 &nbsp;[Latest Release](https://pypi.org/project/csvthd)
- 🧰 &nbsp;[Source Code](https://gitlab.com/DrTexx/csv-transaction-history-detective)
- 🐞 &nbsp;[Issue Tracker](https://gitlab.com/DrTexx/csv-transaction-history-detective/-/issues)
- `🐦 Twitter` &nbsp;[@DrTexx](https://twitter.com/DrTexx)
- `📨 Email` &nbsp;[denver.opensource@tutanota.com](mailto:denver.opensource@tutanota.com)
