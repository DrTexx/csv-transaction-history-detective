# `config.json` Template

## Template

Contents of a `config.json` file

```jsonc
{
    "files": [
        {
            "filepath": "/home/johnsmith/Documents/bof_transactions_2025.csv", # [string] absolute filepath to a CSV file
            "hasHeaderRow": false, # [bool] if your CSV file has a header row
            "dateIdx": 0, # [int] column index of date
            "amountIdx": 1, # [int] column index of amount
            "detailsIdx": 2, # [int] column index of details
            "balanceIdx": 3, # [int] column index of balance at time of transaction
            "accountName": "Bank of Australia" # [string] nickname for account this CSV file relates to
        },
        # ...
    ]
}
```

## Important notes

- Column indices start from zero
  - e.g. if `files[0].dateIdx` is zero, the date will be read from the first column
